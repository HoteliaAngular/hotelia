import { TestBed } from '@angular/core/testing';

import { RateHotelService } from './rate-hotel.service';

describe('RateHotelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RateHotelService = TestBed.get(RateHotelService);
    expect(service).toBeTruthy();
  });
});
