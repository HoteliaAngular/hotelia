import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule} from '@angular/forms';
import { FilterPipe } from '../filter.pipe';
import { MDBBootstrapModule } from 'angular-bootstrap-md';


import { HomeComponent } from '../home/home.component';
import {SearchComponent} from '../search/search.component';
import {HotelListComponent} from './hotel-list/hotel-list.component';
import { IntroComponent } from '../intro/intro.component';
import { HotelItemComponent } from './hotel-item/hotel-item.component';

import {AddHotelService} from './add-hotel.service';
import {RateHotelService} from './rate-hotel.service';
import {SearchHotelService} from './search-hotel.service';
import {TopFiveService} from './top-five.service';
import { HotelDetailService } from './hotel-detail.service';

@NgModule({
  declarations: [
    HomeComponent,
    HotelListComponent,
    SearchComponent,
    IntroComponent,
    HotelItemComponent,
    FilterPipe,
  ],
  imports: [
    CommonModule,
    FormsModule,
    MDBBootstrapModule
  ],
  exports: [
    HomeComponent,
    HotelListComponent,
    SearchComponent,
    IntroComponent,
    HotelItemComponent,
    FilterPipe,
  ]
})
export class HotelModule { }
