import { TestBed } from '@angular/core/testing';

import { HotelDetailService } from './hotel-detail.service';

describe('HotelDetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HotelDetailService = TestBed.get(HotelDetailService);
    expect(service).toBeTruthy();
  });
});
