import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookingComponent } from './booking/booking.component';
import { MyBookingsComponent } from './my-bookings/my-bookings.component';
import { BookHotelService } from './book-hotel.service';


@NgModule({
  declarations: [
    MyBookingsComponent,
    BookingComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    BookingComponent,
    MyBookingsComponent
  ]
})
export class BookingsModule { }
