import { TestBed } from '@angular/core/testing';

import { BookHotelService } from './book-hotel.service';

describe('BookHotelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BookHotelService = TestBed.get(BookHotelService);
    expect(service).toBeTruthy();
  });
});
