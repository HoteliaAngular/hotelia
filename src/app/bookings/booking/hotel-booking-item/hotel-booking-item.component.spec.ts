import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotelBookingItemComponent } from './hotel-booking-item.component';

describe('HotelBookingItemComponent', () => {
  let component: HotelBookingItemComponent;
  let fixture: ComponentFixture<HotelBookingItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotelBookingItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelBookingItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
