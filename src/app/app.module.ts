import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { LayoutModule } from '@angular/cdk/layout';
import {HotelModule} from './hotel/hotel.module';
import { FormsModule } from '@angular/forms';
import { CoreModule} from './core/core.module';
import { BookingsModule } from './bookings/bookings.module';


@NgModule({
  declarations: [
    AppComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    HotelModule,
    CoreModule,
    BookingsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
