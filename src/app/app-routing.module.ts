import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutComponent } from './about/about.component';
import { MyBookingsComponent } from './bookings/my-bookings/my-bookings.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'my-bookings', component: MyBookingsComponent},
  { path: 'about', component: AboutComponent},
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
